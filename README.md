# razor role

Setup razor server

## Requirements

- Debian jessie
  - Ensure following package are installed ```apt-get install -y python python-simplejson lsb-release aptitude```
- Access to Debian and PuppetLabs repos

## Role Variables

```yaml
cts_role_razor:
  razor:
    repo: https://apt.puppetlabs.com/puppetlabs-release-pc1-jessie.deb
    pkg: razor-server
    daemon: razor-server
  pgsql:
    pkg:
      - postgresql
      - postgresql-contrib
      - libpq-dev
    python_library: python-psycopg2
    db: razor_prd
    user: razor
    password: changeme24                # to configure
    daemon: postgresql
    locales:
      - 'en_US.UTF-8'
  dhcp:
    pkg: isc-dhcp-server
    daemon: isc-dhcp-server
    domain_name: localdomain            # to configure
    domain_name_server:
      - 8.8.8.8                         # to configure
    subnet: 172.16.38.0                 # to configure
    subnet_netmask: 255.255.255.0       # to configure
    range: 172.16.38.200 172.16.38.250  # to configure
    router: 172.16.38.1                 # to configure
  tftp:
    daemon: tftpd-hpa
    pkg: tftpd-hpa
    dir: /var/lib/tftpboot
    undionly: http://boot.ipxe.org/undionly.kpxe
    server: 172.16.38.132                # to configure
```

## Dependencies

none, role will setup tftp and dhcp servers

## Example Playbook
```yaml
- hosts: servers
  roles:
     - { role: razor }
```

---
# Razor Usage

## Repos

- Create Debian Centos 7.3.1611
```sh
razor create-repo --name CentOS_7_3 \
    --url http://vault.centos.org/7.3.1611/os/x86_64 \
    --task centos
```

- Create Debian Jessie repo
```sh
razor create-repo --name Debian_jessie_amd64 \
    --task debian \
    --url http://ftp.debian.org/debian/dists/jessie/main/installer-amd64/current/images/netboot/debian-installer/amd64
```

- Create Debian Stretch repo
```sh
razor create-repo --name Debian_stretch_amd64 \
    --task debian \
    --url http://ftp.debian.org/debian/dists/stretch/main/installer-amd64/current/images/netboot/debian-installer/amd64
```

## Tags

- Create tag on uuid for set hostname and static ip (ATTENTION : la valeur de l'uuid DOIT ETRE EN UPPERCASE)
```sh
razor create-tag --name server_name_static \
    --rule '["=", ["fact", "uuid"], "423ffcb8-abc9-a124-36b4-33fbbd25a989"]' # pas bien
```
```sh
razor create-tag --name server_name_static \
    --rule '["=", ["fact", "uuid"], "423FFCB8-ABC9-A124-36B4-33FBBD25A989"]' # good
```


- Create tag for virtual server with dhcp configurations
```sh
razor create-tag --name virtual_dhcp \
     --rule '["=", ["fact", "is_virtual"], true ]'
```

## Policy

- Create policies for Debian Stretch static ip
```sh
razor create-policy --name server_name_static \
    --repo Debian_stretch_amd64 --task debian --broker noop \
    --enabled --hostname 'server_name.domain_name.local' \
    --root-password changeme24 \
    --node-metadata ipaddress=192.168.0.10 \
    --node-metadata netmask=255.255.255.0 \
    --node-metadata gateway=192.168.0.1 \
    --node-metadata nameservers=192.168.0.2 \
    --node-metadata domainname=domain_name.local \
    --tag server_name_static
```

- Create policies for CentOS 7.3 serveurs with dhcp
```sh
razor create-policy --name virtual_dhcp \
    --repo CentOS_7_3 --task centos --broker noop \
    --enabled --hostname 'host${id}.domain_name.local' \
    --root-password changeme24 \
    --tag virtual_dhcp
```

License
-------

BSD
